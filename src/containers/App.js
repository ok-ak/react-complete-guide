//@flow
import React, { Component } from 'react';
import logo from '../assets/logo.svg';
import './App.css';
import Person from '../components/Persons/Person/Person'
import UserInput from '../components/UserInput/UserInput'
import UserOutput from '../components/UserOutput/UserOutput'
import CharComponent from '../components/CharComponent/CharComponent'
import ValidationComponent from '../components/ValidationComponent/ValidationComponent'
import ErrorBoundary from '../components/ErrorBoundary/ErrorBoundary'
import PropTypes from 'prop-types'
import Radium, {StyleRoot} from 'radium'

class App extends Component {
  state : PropTypes = {
    persons: [
      {id: 1, name: 'Mario', age: 1}, 
      {id: 2, name: 'Luigi', age: 2}, 
      {id: 3, name: 'Princess Peach', age: 3 }
    ],
    username: 'Bowser',
    showPersons: false,
    userText: ''
  }

  nameChangeHandler = (event: Object, id: number) => {
    const personIndex = this.state.persons.findIndex(lePerson => {
      return lePerson.id === id
    })

    const person = {
      ...this.state.persons[personIndex]
    }

    person.name = event.target.value

    const persons = [
      ...this.state.persons
    ]

    persons[personIndex] = person

    this.setState({persons: persons})
  }

  usernameChangeHandler = (event: Object) => {
    this.setState({username: event.target.value})
  }

  togglePersonHandler = () => {
    this.setState({showPersons: !this.state.showPersons})
  }

  deletePersonHandler = (index: number) => {
    // const persons = this.state.persons.slice()
    const persons = [...this.state.persons]
    persons.splice(index, 1)
    this.setState({persons: persons})
  }

  userTextChangeHandler = (event: Object) => {
    this.setState({userText: event.target.value})
  }

  deleteCharHandler = (index: number) => {
    const chars = this.state.userText.split('')
    chars.splice(index, 1)
    this.setState({userText: chars.join('')})
  }

  render() {

    const style = {
      backgroundColor: 'green',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ':hover': {
        backgroundColor: 'lightgreen',
        color: 'black'
      }
    }

    let persons = null

    if(this.state.showPersons){
      style.backgroundColor = 'red'
      style[':hover'] = {
        backgroundColor: 'salmon',
        color: 'black'
      }
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
            return(
              <ErrorBoundary key={person.id}>
                <Person 
                  name={person.name}
                  age={person.age}
                  click={() => this.deletePersonHandler(index)}
                  changed={(event) => this.nameChangeHandler(event, person.id)}
                />
              </ErrorBoundary>
            )
          })}
        </div>
      )
    }

    let chars = (
      <div>
        {
          this.state.userText.split('').map((char, index) => {
            return(
              <CharComponent
                char={char}
                index={index}
                click={() => this.deleteCharHandler(index)}
              />
            )
          })
        }
      </div>
    )

    const classes = []
    if(this.state.persons.length <= 2) {
      classes.push('red')
    }
    if(this.state.persons.length <= 1) {
      classes.push('bold')
    }

    return (
      <StyleRoot>
        <div className="App">
          <h1>Hello! I am a React App</h1>
          <p className={classes.join(' ')}>THis is really working</p>
          <button  
            style={style}
            onClick={this.togglePersonHandler}>Toggle Persons</button>
          {persons}
          <div>
            <UserInput
              usernameChange={this.usernameChangeHandler.bind(this)}
              username={this.state.username}></UserInput>
            <UserOutput username={this.state.username}></UserOutput>
          </div>
          <div>
            <ValidationComponent
              userTextChanged = {(event) => this.userTextChangeHandler(event)}
              userText = {this.state.userText}
            >
            </ValidationComponent>
          </div>
          {chars}
        </div>
      </StyleRoot>
    );
    // return React.createElement('div', null, React.createElement('h1', null, 'Yaaaaar I am a React App!!!!!'))
  }
}



export default Radium(App);
