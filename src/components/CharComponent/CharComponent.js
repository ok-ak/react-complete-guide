//@flow
import React from 'react'
import PropTypes from 'prop-types'

const charComponent = (props: PropTypes) => {

  const style = {
    display: 'inline-block',
    padding: '16px',
    textAlign: 'center',
    margin: '16px',
    border: '1px solid black'
  }

  return(
    <div className='CharComponent' style={style} onClick={props.click}>
      {props.char}
    </div>
  )
}

export default charComponent