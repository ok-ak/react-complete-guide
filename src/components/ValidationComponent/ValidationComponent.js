//@flow
import React from 'react'
import PropTypes from 'prop-types'

const validationComponent = (props: PropTypes) => {
  
  const minTextLength = 5
  const maxTextLength = 10

  const style = {
    margin: '0 auto',
    width: '200px',
    backgroundColor: 'white',
    font: 'inherit',
    border: '1px solid pink',
    padding: '8px',
    cursor: 'pointer'
  }

  const userTextLength = props.userText.split('').length

  let getUserTextAnalysis = (userText) => {
      if(userTextLength == 0){
        return 'No comment : )'
      } else if (userTextLength > maxTextLength){
        return 'Too many characters!'
      } else if (userTextLength < minTextLength){
        return 'Not enough characters!'
      } else {
        return 'Just the right amount of characters!'
      }
  }

  return(
    <div className="ValidationComponent" style={style}>
    <h6>I am a validation component!</h6>
      <input type='text' onChange={props.userTextChanged} value={props.userText}></input>
      <p>Your input is: {userTextLength} chars long</p>
      <p>{getUserTextAnalysis(props.userText)}</p>
    </div>
  )
}

export default validationComponent