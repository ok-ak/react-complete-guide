#WHAT IS THIS?!?
Its a student(me) working folder following the course content found at [https://www.udemy.com/react-the-complete-guide-incl-redux](https://www.udemy.com/react-the-complete-guide-incl-redux)

This working folder(solution) is current with course material as of Sept 2018 but may become out of sync as the course material is updated in the future.

Just a fyi, my solution may not jive 100% with the lecture material since I tend to modify things to make course material more interesting. Its the little things in life that make it fun right?


##Disclaimer!!!
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

