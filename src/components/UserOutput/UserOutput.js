//@flow
import React from 'react'
import PropTypes from 'prop-types'

const userOutput = (props: PropTypes) => {

  let shuffle = (a: number[]): number[] => {
    for (let i: number = a.length - 1; i > 0; i--) {
        const j: number = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

  let getRandomHexColorString = (): string => {
    const minColorInt = 0
    const maxColorInt = 255
    let rgb = new Array(3).fill(0)
    for(let i = 0; i < rgb.length; i++){
      if(rgb[0] + rgb[1] > maxColorInt){
        rgb[i] = Math.floor(Math.random() * (maxColorInt - Math.floor(maxColorInt/2) + 1)) + Math.floor(maxColorInt/2)
      } else {
        rgb[i] = Math.floor(Math.random() * (maxColorInt - minColorInt + 1)) + minColorInt
      }
    }
    shuffle(rgb)
    let rgbHex = rgb.map(x => x.toString(16))
    const rgbHexString = '#' + rgbHex.join('')
    console.log(rgbHexString)
    return rgbHexString
  }

  const style = {
    backgroundColor: getRandomHexColorString(),
    color: getRandomHexColorString(),
    textAlign: 'center'
  }

  return(
    <div style={style}>
      <p>Mah username is:</p>
      <p>{props.username}</p>
    </div>
  )
}

export default userOutput