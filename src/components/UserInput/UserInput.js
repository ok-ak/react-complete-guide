//@flow
import React from 'react'
import PropTypes from 'prop-types'

const userInput = (props: PropTypes) => {
  return (
    <input type='text' onChange={props.usernameChange} value={props.username}></input>
  )
}

export default userInput